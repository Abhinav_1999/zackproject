import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Exhibtioner1Page } from './exhibtioner1.page';

const routes: Routes = [
  {
    path: '',
    component: Exhibtioner1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Exhibtioner1PageRoutingModule {}
