import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Exhibtioner1PageRoutingModule } from './exhibtioner1-routing.module';

import { Exhibtioner1Page } from './exhibtioner1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Exhibtioner1PageRoutingModule
  ],
  declarations: [Exhibtioner1Page]
})
export class Exhibtioner1PageModule {}
