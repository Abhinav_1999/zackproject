import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Katalog2PageRoutingModule } from './katalog2-routing.module';

import { Katalog2Page } from './katalog2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Katalog2PageRoutingModule
  ],
  declarations: [Katalog2Page]
})
export class Katalog2PageModule {}
