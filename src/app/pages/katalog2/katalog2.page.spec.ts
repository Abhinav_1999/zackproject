import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Katalog2Page } from './katalog2.page';

describe('Katalog2Page', () => {
  let component: Katalog2Page;
  let fixture: ComponentFixture<Katalog2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Katalog2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Katalog2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
