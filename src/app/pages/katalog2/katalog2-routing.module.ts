import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Katalog2Page } from './katalog2.page';

const routes: Routes = [
  {
    path: '',
    component: Katalog2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Katalog2PageRoutingModule {}
