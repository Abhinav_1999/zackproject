import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Erfassen1Page } from './erfassen1.page';

const routes: Routes = [
  {
    path: '',
    component: Erfassen1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Erfassen1PageRoutingModule {}
