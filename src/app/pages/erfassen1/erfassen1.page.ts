import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-erfassen1',
  templateUrl: './erfassen1.page.html',
  styleUrls: ['./erfassen1.page.scss'],
})
export class Erfassen1Page implements OnInit {

  constructor(private route: Router) { }


  goAussteller(){
    this.route.navigate(['erfassen-austeller'])
  }

  goAddNote(){
    this.route.navigate(['add-note-page'])
  }

  ngOnInit() {
  }

}
