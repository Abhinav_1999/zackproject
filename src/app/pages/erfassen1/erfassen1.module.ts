import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Erfassen1PageRoutingModule } from './erfassen1-routing.module';

import { Erfassen1Page } from './erfassen1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Erfassen1PageRoutingModule
  ],
  declarations: [Erfassen1Page]
})
export class Erfassen1PageModule {}
