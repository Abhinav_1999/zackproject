import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NotePagePage } from './note-page.page';

describe('NotePagePage', () => {
  let component: NotePagePage;
  let fixture: ComponentFixture<NotePagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotePagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NotePagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
