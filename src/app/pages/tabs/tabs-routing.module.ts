import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
     
      {
        path: 'home',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../exhibition-selection/exhibition-selection.module').then(m => m.ExhibitionSelectionPageModule)
          }
        ]
      }, {
        path: 'erfassen1',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../erfassen1/erfassen1.module').then(m => m.Erfassen1PageModule)
          }
        ]
      },
      
      {
        path: '',
        redirectTo: '/tabs/home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}


