import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-audio-note-page',
  templateUrl: './add-audio-note-page.page.html',
  styleUrls: ['./add-audio-note-page.page.scss'],
})
export class AddAudioNotePagePage implements OnInit {

  constructor(private route: Router) { }

  return(){
    this.route.navigate(['add-note-page'])
  }

  go(){
    this.route.navigate(['katalog'])
  }

  ngOnInit() {
  }

}
