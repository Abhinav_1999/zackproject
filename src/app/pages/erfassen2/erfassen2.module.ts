import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Erfassen2PageRoutingModule } from './erfassen2-routing.module';

import { Erfassen2Page } from './erfassen2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Erfassen2PageRoutingModule
  ],
  declarations: [Erfassen2Page]
})
export class Erfassen2PageModule {}
