import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Erfassen2Page } from './erfassen2.page';

const routes: Routes = [
  {
    path: '',
    component: Erfassen2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Erfassen2PageRoutingModule {}
