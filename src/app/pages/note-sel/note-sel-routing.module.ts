import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NoteSelPage } from './note-sel.page';

const routes: Routes = [
  {
    path: '',
    component: NoteSelPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NoteSelPageRoutingModule {}
