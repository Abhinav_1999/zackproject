import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-note-sel',
  templateUrl: './note-sel.page.html',
  styleUrls: ['./note-sel.page.scss'],
})
export class NoteSelPage implements OnInit {

  constructor(private route: Router) { }

  go(){
    this.route.navigate(['add-note-page'])
  }

  ngOnInit() {
  }

}
 