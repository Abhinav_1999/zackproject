import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../api.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-katalog',
  templateUrl: './katalog.page.html',
  styleUrls: ['./katalog.page.scss'],
})
export class KatalogPage implements OnInit {
  pinwandArray:any=[];
  isPinwand:boolean=false;
  slideOpts={
    initialSlide: 0,
    slidesPerView:3
  }

  constructor(private route: Router, public api:ApiService, public storage:Storage) { }
  return(){
    this.route.navigate(['add-audio-note-page'])
  }

  ngOnInit() {
   
    this.storage.get('pinnwand').then((pinnwand)=>{
     this.pinwandArray=pinnwand;
     console.log(this.pinwandArray);
     if(this.pinwandArray==null||this.pinwandArray==undefined||this.pinwandArray.length==0){
      this.isPinwand=false;
      }else{
        this.isPinwand=true;
      }
      
    });
  }

  addPinnwand(){
    console.log("hey");
    this.api.Navigate('add-page');

  }
  


  

}
