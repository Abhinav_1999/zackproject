import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddTextnotePagePage } from './add-textnote-page.page';

const routes: Routes = [
  {
    path: '',
    component: AddTextnotePagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddTextnotePagePageRoutingModule {}
