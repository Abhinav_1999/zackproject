import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AnfragenSeitePage } from './anfragen-seite.page';

describe('AnfragenSeitePage', () => {
  let component: AnfragenSeitePage;
  let fixture: ComponentFixture<AnfragenSeitePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnfragenSeitePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AnfragenSeitePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
