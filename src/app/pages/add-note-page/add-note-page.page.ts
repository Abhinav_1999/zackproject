import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-add-note-page',
  templateUrl: './add-note-page.page.html',
  styleUrls: ['./add-note-page.page.scss'],
})
export class AddNotePagePage implements OnInit {

  constructor(private route: Router) { }

  go(){
    this.route.navigate(["add-audio-note-page"])
  }

  goTextnote(){
    this.route.navigate(['add-textnote-page'])
  }


return(){
  this.route.navigate(['add-page'])
}

  ngOnInit() {
  }

}
