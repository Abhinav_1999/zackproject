import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ErfassenAustellerPage } from './erfassen-austeller.page';

const routes: Routes = [
  {
    path: '',
    component: ErfassenAustellerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ErfassenAustellerPageRoutingModule {}
