import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NotesMixedPageRoutingModule } from './notes-mixed-routing.module';

import { NotesMixedPage } from './notes-mixed.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotesMixedPageRoutingModule
  ],
  declarations: [NotesMixedPage]
})
export class NotesMixedPageModule {}
