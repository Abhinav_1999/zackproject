import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bl-katalog',
  templateUrl: './bl-katalog.page.html',
  styleUrls: ['./bl-katalog.page.scss'],
})
export class BlKatalogPage implements OnInit {

  constructor(private route: Router) { }

  return(){
    this.route.navigate(['add-note-page'])
  }

  goAnfrage(){
    this.route.navigate(['anfragen-seite'])
  }

  goAddpage(){
    this.route.navigate(['add-page'])
  }

  ngOnInit() {
  }

}
