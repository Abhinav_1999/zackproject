import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-exhibition-selection',
  templateUrl: './exhibition-selection.page.html',
  styleUrls: ['./exhibition-selection.page.scss'],
})
export class ExhibitionSelectionPage implements OnInit {
  slideOpts = {
    initialSlide: 0,
    slidesPerView:3
  };
  constructor() { }
 
  ngOnInit() {
  }

}
