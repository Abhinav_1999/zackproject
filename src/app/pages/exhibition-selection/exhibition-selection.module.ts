import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExhibitionSelectionPageRoutingModule } from './exhibition-selection-routing.module';

import { ExhibitionSelectionPage } from './exhibition-selection.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExhibitionSelectionPageRoutingModule
  ],
  declarations: [ExhibitionSelectionPage]
})
export class ExhibitionSelectionPageModule {}
