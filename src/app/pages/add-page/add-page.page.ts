import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-add-page',
  templateUrl: './add-page.page.html',
  styleUrls: ['./add-page.page.scss'],
})
export class AddPagePage implements OnInit {

  constructor(private route: Router, public api:ApiService) { }


  goNote(){
    this.route.navigate(['add-note-page'])
  }

  goProduct(){
    this.route.navigate(['add-product'])
  }

  ngOnInit() {
  }

  back(){
    this.api.Navigateback('katalog');

  }

}
