import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    loadChildren: () => import('./pages/tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'index',
    loadChildren: () => import('./index/index.module').then( m => m.IndexPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'signup',
    loadChildren: () => import('./pages/signup/signup.module').then( m => m.SignupPageModule)
  },
  {
    path: 'exhibition-selection',
    loadChildren: () => import('./pages/exhibition-selection/exhibition-selection.module').then( m => m.ExhibitionSelectionPageModule)
  },
  {
    path: 'exhibitors',
    loadChildren: () => import('./pages/exhibitors/exhibitors.module').then( m => m.ExhibitorsPageModule)
  },
  {
    path: 'erfassen1',
    loadChildren: () => import('./pages/erfassen1/erfassen1.module').then( m => m.Erfassen1PageModule)
  },
  {
    path: 'erfassen2',
    loadChildren: () => import('./pages/erfassen2/erfassen2.module').then( m => m.Erfassen2PageModule)
  },
  {
    path: 'erfassen-austeller',
    loadChildren: () => import('./pages/erfassen-austeller/erfassen-austeller.module').then( m => m.ErfassenAustellerPageModule)
  },
  {
    path: 'katalog',
    loadChildren: () => import('./pages/katalog/katalog.module').then( m => m.KatalogPageModule)
  },
  {
    path: 'add-page',
    loadChildren: () => import('./pages/add-page/add-page.module').then( m => m.AddPagePageModule)
  },
  {
    path: 'add-note-page',
    loadChildren: () => import('./pages/add-note-page/add-note-page.module').then( m => m.AddNotePagePageModule)
  },
  {
    path: 'add-textnote-page',
    loadChildren: () => import('./pages/add-textnote-page/add-textnote-page.module').then( m => m.AddTextnotePagePageModule)
  },
  {
    path: 'add-audio-note-page',
    loadChildren: () => import('./pages/add-audio-note-page/add-audio-note-page.module').then( m => m.AddAudioNotePagePageModule)
  },
  {
    path: 'anfragen-seite',
    loadChildren: () => import('./pages/anfragen-seite/anfragen-seite.module').then( m => m.AnfragenSeitePageModule)
  },
  {
    path: 'add-product',
    loadChildren: () => import('./pages/add-product/add-product.module').then( m => m.AddProductPageModule)
  },
  {
    path: 'exhibtioner1',
    loadChildren: () => import('./pages/exhibtioner1/exhibtioner1.module').then( m => m.Exhibtioner1PageModule)
  },
  {
    path: 'note-page',
    loadChildren: () => import('./pages/note-page/note-page.module').then( m => m.NotePagePageModule)
  },
  {
    path: 'adjustments',
    loadChildren: () => import('./pages/adjustments/adjustments.module').then( m => m.AdjustmentsPageModule)
  },
  {
    path: 'note-sel',
    loadChildren: () => import('./pages/note-sel/note-sel.module').then( m => m.NoteSelPageModule)
  },
  {
    path: 'audionote2',
    loadChildren: () => import('./pages/audionote2/audionote2.module').then( m => m.Audionote2PageModule)
  },
  {
    path: 'audionote3',
    loadChildren: () => import('./pages/audionote3/audionote3.module').then( m => m.Audionote3PageModule)
  },
  {
    path: 'katalog2',
    loadChildren: () => import('./pages/katalog2/katalog2.module').then( m => m.Katalog2PageModule)
  },
  {
    path: 'profil',
    loadChildren: () => import('./pages/profil/profil.module').then( m => m.ProfilPageModule)
  },
  {
    path: 'bl-katalog',
    loadChildren: () => import('./pages/bl-katalog/bl-katalog.module').then( m => m.BlKatalogPageModule)
  },  {
    path: 'notes-mixed',
    loadChildren: () => import('./notes-mixed/notes-mixed.module').then( m => m.NotesMixedPageModule)
  },
  {
    path: 'exhibitors-list',
    loadChildren: () => import('./exhibitors-list/exhibitors-list.module').then( m => m.ExhibitorsListPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
